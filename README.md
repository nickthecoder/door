A web based camera monitor with proximity sensor, and door bell sensor.

Installation
============

Install the Raspberry Pi OS, but do NOT use the most up to date version,
because it uses a new technology for the camera, which doesn't play nicely with mjpg_streamer.
I used 2021-10-30-raspios-bullseye-armhf-lite.zip, which you can get from here :

    http://www.cs.tohoku-gakuin.ac.jp/pub/Linux/RaspBerryPi/

Unzip it, and copy it to an SD Card (there are lots of help online how to do this!)

I recommend compiling this project on a regular PC/laptop, and copying the result to the raspberry PI.
It is quick and easy (assuming you have a linux computer at hand). If you only have a Windows machine,
then, sorry, I can't help you, I haven't touched Windows in decades.

    git clone https://gitlab.com/nickthecoder/door.git
    cd door
    ./gradlew
    
    # Copy the application to the Pi :
    # Replace the address with your pi's address, or its hostname (such as foo.local).
    scp -r build/install/door pi@192.168.1.63:/home/pi/

The rest can be done on the Raspberry Pi.
    
    ssh pi@192.168.1.63

The data and application are in different places, so create a couple of directories for the data :

    mkdir /home/pi/data
    mkdir /home/pi/data/photos
    
We need to install Java 11, but currently OpenJDK doesn't support ARM6 (the processor used on Raspberry Pi Zero).
If you are using a "full sized" Raspberry Pi version 3 or 4, then I think you can use the OpenJDK supplied by
the operating system :

    sudo apt-get install openjdk-11-jdk

But if you have a Pi Zero, follow these instructions :

    https://pi4j.com/documentation/java-installation/

Now install mjgp_streamer (which gives access to the camera via web pages) :

First, enable the camera :

    sudo raspi-config
    # Enable the camera

    # Install the build tools
    sudo apt-get install cmake gcc g++ git
    # I'm not 100% which of the following are required (maybe only the first???).
    sudo apt-get install libjpeg-dev libgphoto2-dev libprotobuf-dev libprotobuf-c-dev libraspberrypi-dev
    
    cd
    git clone https://github.com/jacksonliam/mjpg-streamer.git
    cd mjpg-streamer/mjpg-streamer-experimental/
    make

At this point, we have all the software required, we just need to start up both parts.

Set up systemd services, which make it easy to start and stop the two services.

    sudo cp /home/pi/door/*.service /etc/systemd/system
    sudo systemctl start camera.service
    sudo systemctl start door.service

Give each service a minute to startup, then look at the logs to see if anything is wrong :

    sudo journalctl -u camera.service
    sudo journalctl -u door.service

To make the services automatically start when the pi starts :

    sudo systemctl enable camera.serivce
    sudo systemctl enable door.service
    
Test out the application, by visiting the website. Replace the address with the address of your pi,
or use its hostname (such as foo.local).

    http://192.168.1.63

