// door

plugins {
    kotlin("jvm") version "1.9.10"
    application
}

repositories {
    mavenCentral()
}

val doorVersion: String by project
val exposedVersion: String by project
val ktorVersion: String by project
val slf4jVersion: String by project
val pi4jVersion: String by project
val sqliteVersion: String by project
val commonsVersion: String by project

version=doorVersion

dependencies {
    // Web server
    implementation("io.ktor:ktor-server-core:$ktorVersion")
    implementation("io.ktor:ktor-server-netty:$ktorVersion")
    implementation("io.ktor:ktor-html-builder:$ktorVersion")

    // Logging
    implementation("org.slf4j:slf4j-simple:${slf4jVersion}")

    // GPIO
    implementation("com.pi4j:pi4j-core:${pi4jVersion}")
    implementation("com.pi4j:pi4j-plugin-raspberrypi:${pi4jVersion}")
    implementation("com.pi4j:pi4j-plugin-pigpio:${pi4jVersion}")

    // Database
    implementation("org.jetbrains.exposed:exposed-core:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-dao:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-jdbc:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-java-time:$exposedVersion")
    implementation("org.xerial:sqlite-jdbc:$sqliteVersion")

    // Utilities
    implementation("commons-io:commons-io:${commonsVersion}")

}

application {
    mainClass.set("uk.co.nickthecoder.door.Door")
}


task<Exec>("publishZip") {
    dependsOn(":distZip")
    commandLine("cp", "build/distributions/door-${doorVersion}.zip", "../download/")
}

task<Exec>("ntc") {
    dependsOn(":publishZip")
    commandLine("echo", "Done" )
}

defaultTasks("installDist")
