Door
====

Build a make-shift project board to mount the pieces to
    Later build a 3D printed enclosure for the project.
    Extra face plate for the camera and IR LEDs

Bell board
    12V input
    2 connectors from switch

    3 pin connector to outside : GND, 12V, BELL DATA (3.3)

    Buck to 3.3V
    Pull-down resistor 10K? (switch data to gnd)
        Test the bell still works!
    Glue it to the battery cover

Create a "break out" board, which the pi attaches to.
    Simple headers, making two layers?

    2 terminals for the 12 V input
    +1 terminal for the door bell data.

    2 terminals for 12V output for LEDs
    3 terminals for the PIR module (0,5V, data)

    Pull down resistor for the door bell pin.
    Relay? and transistor&resistor(s) for the LEDs
    Buck to 5 V for the Pi
    Reserve space for DAC and light sensor.
        Or a simple capacitance measurement instead of the DAC???
        https://arduino.stackexchange.com/questions/29074/use-photocell-as-digital-input
        OR patch into the camera LEDs, and turn the main LEDs on at the same time.

Power pi via 12V and buck circuit.

Add proximity sensor using GPIO

Get door bell working with buck circuit and 12V supply
Add door bell sensor using GPIO

Care-taking :
    Remove photos which aren't in the database
    Remove events, and their photos which are older than N days.

Create an Android app which is notified on each new event.
    Begin a connection, but never complete it from the server? (So we don't need to poll).



