package uk.co.nickthecoder.door

import io.ktor.html.*
import kotlinx.html.*

open class DoorTemplate(val tab: String = "") : Template<HTML> {

    // HEAD SECTION
    val pageTitle = Placeholder<TITLE>()

    /**
     * The main content of the page
     */
    val content = Placeholder<FlowContent>()

    override fun HTML.apply() {

        head {
            title { insert(pageTitle) }
            meta(name = "viewport", content = "width=device-width, initial-scale=1")
            link(rel = "stylesheet", type = "text/css", href = "/style/style.css")
            link(rel = "icon", href = "/style/icon.png")
        }

        body {
            div {
                id = "whole"

                div {
                    id = "navigation"
                    div("actions") {
                        form(action = "/manual", method = FormMethod.post) {
                            button(type = ButtonType.submit) { img(src = "/style/manual.png") }
                        }
                        if (GPIO.lights) {
                            form(action = "/lightsOff", method = FormMethod.post) {
                                button(type = ButtonType.submit) {
                                    img(src = "/style/lightsOn.png")
                                }
                            }
                        } else {
                            form(action = "/lightsOn", method = FormMethod.post) {
                                button(type = ButtonType.submit) {
                                    img(src = "/style/lightsOff.png")
                                }
                            }
                        }
                    }
                    ul("nav") {
                        li(select(tab, SNAPSHOT)) { a("/") { +SNAPSHOT } }
                        li(select(tab, STREAM)) { a("/stream") { +STREAM } }
                        li(select(tab, HISTORY)) { a("/history") { +HISTORY } }
                    }

                }
                div {
                    id = "content"

                    insert(content)
                }
            }
        }
    }

    private fun select(a: String, b: String) = if (a == b) "selected" else ""

    companion object {
        const val SNAPSHOT = "SNAPSHOT"
        const val STREAM = "STREAM"
        const val HISTORY = "HISTORY"
    }
}
