package uk.co.nickthecoder.door

import org.jetbrains.exposed.sql.Database
import uk.co.nickthecoder.door.Door.main
import uk.co.nickthecoder.door.Door.start
import uk.co.nickthecoder.door.util.createTables
import java.io.File
import java.sql.DriverManager
import java.time.format.DateTimeFormatter

/**
 * Rather than parse a configuration file, all the configurable values are simple vars.
 * If you need to change any of the values:
 *
 * Either change the source code, and recompile your custom version of the Door project.
 *
 * Or create your own entry point (instead of Door.[main]), and alter the values before calling
 * Door.[start].
 */
object Door {

    /**
     * The port number for the web server. 80 is the "normal" port number for web pages,
     * but can only be used if the program is running as root.
     */
    var port = 8081

    /**
     * The location of the sqlite database which holds data about the photos taken
     */
    var sqliteDB = File("door.db")

    /**
     * The directory where all photos are stored
     */
    var photosDirectory = File("photos")

    /**
     * The url to get a single image from the camera. See mjpg_streamer
     */
    var snapshotURL = "http://192.168.1.63:8080/?action=snapshot"

    /**
     * The url to stream a continuous set of images from the camera. See mjpg_streamer
     */
    var streamURL = "http://192.168.1.63:8080/?action=stream"

    /**
     * When an event is triggered, how long should we keep taking pictures?
     */
    var eventDurationMillis = 5000L

    /**
     * How much time between each photo?
     */
    var photoSpacingMillis = 1000L

    /**
     * How much time after the last photo is taken should we remain open to extending the event
     * if new triggers are fired.
     */
    var eventTailMillis = 1000L

    /**
     * Used as part of a photo's filename.
     */
    var dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd kk:mm:ss")

    /**
     * How long to wait when getting a photo from mjpg_streamer.
     */
    var connectionTimeoutMillis = 5000

    /**
     * How long to wait when getting a photo from mjpg_streamer.
     */
    var readTimeoutMillis = 5000

    /**
     * The BCM pin numbers for the GPIO pin for the bell
     */
    var bellPin = 23

    /**
     * The BCM pin numbers for the GPIO pin for the proximity sensor
     */
    var proximityPin = 24

    /**
     * The BCM pin numbers for the GPIO pin to turn on the lights
     */
    var lightsPin = 17

    /**
     * Amount of time to keep the lights on when lights are lit manually
     * via the web site.
     */
    var manualLightMillis = 1000L * 60 * 1

    /**
     * The sleep delay of the caretaker task. Default is 24 hours.
     */
    var caretakerSleepMillis = 24 * 60 * 60 * 1000L

    /**
     * The number of days of photos to keep.
     * 0 To only keep today's photos and events.
     */
    var historyDays = 7

    @JvmStatic
    fun start() {
        if (!sqliteDB.exists()) {
            println("Database $sqliteDB does not exist. Creating empty database.")
            createTables()
        }

        println("Opening sqlite database $sqliteDB")
        Database.connect({ DriverManager.getConnection("jdbc:sqlite:$sqliteDB") })

        println("Starting Caretaker")
        Caretaker.start()

        try {
            println("Starting GPIO")
            GPIO.start()
        } catch (e: Exception) {
            // GPIO will only work on a RaspberryPI, so testing from a "regular" PC/laptop,
            // we expect it to fail.
            println("ERROR GPIO not initialised. Bell and proximity will not work")
        }

        println("Starting Door web site on port $port")
        WebServer(port).create().start(wait = false)
    }

    @JvmStatic
    fun main(vararg args: String) {

        for (arg in args) {
            val (argName, value) = if (arg.startsWith("--")) {
                val eq = arg.indexOf("=")
                if (eq > 0) {
                    Pair(arg.substring(2, eq), arg.substring(eq + 1))
                } else {
                    Pair(arg.substring(2), null)
                }
            } else {
                Pair(null, null)
            }

            when (argName) {
                "help" -> {
                    println(helpText)
                    System.exit(0)
                }
                "port" -> port = value!!.toInt()
                "photoDir" -> photosDirectory = File(value!!)
                "db" -> sqliteDB = File(value!!)

            }
        }

        start()
    }

    private val helpText = """
            door [options...]
            
            OPTIONS :
            
                --port=PORT_NUMBER       The port number of the web server
                
                --photoDir=DIRECTORY     The directory where photos are stored
                
                --db=DATABASE_FILE       The sqlite database file.
            
        """.trimIndent()
}
