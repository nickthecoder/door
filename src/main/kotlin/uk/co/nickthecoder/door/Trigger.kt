package uk.co.nickthecoder.door

import org.apache.commons.io.FileUtils
import org.jetbrains.exposed.sql.transactions.transaction
import java.io.File
import java.net.URL
import java.time.LocalDateTime
import java.util.*

enum class TriggerType {
    BELL,
    PROXIMITY,
    MANUAL
}

private var eventThread: EventThread? = null

/**
 * Trigger a new event. Called when the bell is pressed, the proximity sensor is activated, or
 * the "take photo" button is pressed from the web site.
 */
@Synchronized
fun trigger(type: TriggerType) {
    if (eventThread?.extendEvent(type) != true) {
        // Could not extend an existing event, so let's create a new one.
        try {
            var event: Event? = null
            transaction {
                event = Event.new {
                    time = LocalDateTime.now()

                    bell = false
                    proximity = false
                    manual = false
                    when (type) {
                        TriggerType.BELL -> bell = true
                        TriggerType.PROXIMITY -> proximity = true
                        TriggerType.MANUAL -> manual = true
                    }
                }
            }
            event?.let {
                println("Event id = ${it.id.value}")
                eventThread = EventThread(it.id.value).apply { start() }
            }
        } catch (e: Exception) {
            println("Failed to create a new $type event : $e")
        }
    }
}


class EventThread(private val eventID: Int) : Thread("event") {

    @Volatile
    private var continuing = true

    @Volatile
    private var ended = false

    @Synchronized
    fun extendEvent(type: TriggerType): Boolean {
        if (ended && !continuing) return false

        continuing = true
        transaction {
            Event.findById(eventID)?.let {
                when (type) {
                    TriggerType.BELL -> it.bell = true
                    TriggerType.PROXIMITY -> it.proximity = true
                    TriggerType.MANUAL -> it.manual = true
                }
            }
        }
        return true
    }

    @Synchronized
    private fun isContinuing(): Boolean {
        val result = continuing
        continuing = false
        return result
    }

    override fun run() {
        println("Started EventThread")
        while (isContinuing()) {
            ended = false
            val start = Date().time
            while (Date().time - start < Door.eventDurationMillis) {

                takePhoto()

                sleep(Door.photoSpacingMillis)
            }
            sleep(Door.eventTailMillis)
            ended = true
        }
        println("Ended EventThread")
    }

    private fun takePhoto() {
        println("Taking photo")
        val now = LocalDateTime.now()
        val photoName = "${Door.dateTimeFormatter.format(now)}.jpg"
        val path = File(Door.photosDirectory, photoName)

        try {
            FileUtils.copyURLToFile(
                URL(Door.snapshotURL), path, Door.connectionTimeoutMillis, Door.readTimeoutMillis
            )

            transaction {
                Event.findById(eventID)?.let { theEvent ->
                    Photo.new {
                        event = theEvent
                        filename = path.path
                    }
                }
            }
        } catch (e: Exception) {
            println("Event Photo failed : $e")
        }
    }
}
