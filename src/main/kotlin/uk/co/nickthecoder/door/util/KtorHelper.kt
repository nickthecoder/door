package uk.co.nickthecoder.door.util

import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*

fun ApplicationCall.requiredIntParameter(name: String, parameters: Parameters? = null): Int {
    return (this.parameters[name] ?: parameters?.get(name))?.toIntOrNull()
        ?: throw NotFoundException("Parameter $name not given")
}


fun ApplicationCall.optionalIntParameter(
    name: String,
    defaultValue: Int,
    parameters: Parameters? = null
): Int {
    return (this.parameters[name] ?: parameters?.get(name))?.toIntOrNull() ?: defaultValue
}
