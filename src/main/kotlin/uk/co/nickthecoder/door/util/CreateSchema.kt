package uk.co.nickthecoder.door.util

import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction
import uk.co.nickthecoder.door.Door
import uk.co.nickthecoder.door.Events
import uk.co.nickthecoder.door.Photos
import java.sql.DriverManager

/**
 * Creates the tables in the (empty?) database.
 *
 * Prints the resulting schema.
 */
fun main() {
    createTables()
    dumpSchema()
}

fun createTables() {

    val db = Database.connect({ DriverManager.getConnection("jdbc:sqlite:${Door.sqliteDB}") })
    transaction(db) {
        SchemaUtils.create(
            Events,
            Photos
        )
    }

}

fun dumpSchema() {

    ProcessBuilder(
        "sh",
        "-c",
        "sqlite3 ${Door.sqliteDB} .schema | sed -e 's/(/(\\n    /'  -e 's/, /,\\n    /g' -e 's/);/\\n);\\n/' -e 's/REFERENCES/\\n        REFERENCES/g' -e 's/CONSTRAINT/\\n    CONSTRAINT/g'\n"
    ).apply {
        inheritIO()
    }.start()
}
