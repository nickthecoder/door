package uk.co.nickthecoder.door.util

import com.pi4j.io.gpio.digital.Digital
import com.pi4j.io.gpio.digital.DigitalStateChangeListener

/**
 * The Pi4J Library uses varargs as the parameter for addListener, which doesn't play well with
 * Kotlin's lambdas.
 */
fun Digital<*, *, *>.listen(listener: DigitalStateChangeListener) {
    addListener(listener)
}
