package uk.co.nickthecoder.door

import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.`java-time`.datetime

object Events : IntIdTable("event", "eventID") {

    val time = datetime("time")

    val bell = bool("rang")
    val proximity = bool("proximity")
    val manual = bool("manual") // Photo taken manually via the web site.
}

/**
 * An event is a short period of time, when someone is at the front door.
 * An event can be created due to the proximity sensor, or the door bell pressed,
 * or manually via the web site.
 *
 * When an event is created, several photos can be taken.
 *
 * If multiple triggers are fired in quick succession, then they are all combined into a single event.
 * For example, if the proximity sensor goes off, then the door bell is rung, this will result in a
 * SINGLE event (with [bell]=true and [proximity]=true).
 */
class Event(id: EntityID<Int>) : Entity<Int>(id) {
    companion object : EntityClass<Int, Event>(Events)

    var time by Events.time

    var bell by Events.bell
    var proximity by Events.proximity
    var manual by Events.manual

    val photos by Photo referrersOn Photos.eventID
}

object Photos : IntIdTable("photo", "photoID") {
    val eventID = reference("eventID", Events)

    val filename = varchar("filename", 100)
}

class Photo(id: EntityID<Int>) : Entity<Int>(id) {
    companion object : EntityClass<Int, Photo>(Photos)

    var event by Event referencedOn Photos.eventID
    var filename by Photos.filename
}
