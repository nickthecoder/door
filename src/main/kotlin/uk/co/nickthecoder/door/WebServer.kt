package uk.co.nickthecoder.door

import io.ktor.application.*
import io.ktor.features.*
import io.ktor.html.*
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.util.*
import kotlinx.html.*
import org.jetbrains.exposed.sql.SortOrder
import uk.co.nickthecoder.door.util.optionalIntParameter
import uk.co.nickthecoder.door.util.requiredIntParameter
import uk.co.nickthecoder.door.util.suspendTransaction
import java.io.File
import java.time.Duration
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class WebServer(private val port: Int) {

    fun create(): ApplicationEngine {

        return embeddedServer(Netty, port) {

            install(DefaultHeaders)
            install(ContentNegotiation)
            install(CachingHeaders) {
                options { outgoingContent ->
                    val ct = outgoingContent.contentType
                    when (ct) {
                        null -> null

                        ContentType.Text.JavaScript,
                        ContentType.Text.CSS
                        -> CachingOptions(CacheControl.MaxAge(maxAgeSeconds = 7 * DAY))

                        else -> {
                            if (ct.match(ContentType.Image.Any) || ct.match(ContentType.Audio.Any)) {
                                CachingOptions(CacheControl.MaxAge(maxAgeSeconds = 7 * DAY))
                            } else {
                                null
                            }
                        }
                    }
                }
            }
            install(IgnoreTrailingSlash)
            install(Compression)

            routing {
                staticResources("/style", "${this@WebServer.javaClass.packageName}.style")

                get("/") { snapshot(call) }
                get("/stream") { stream(call) }
                get("/history") { history(call) }
                post("/manual") { manual(call) }
                get("/photo/{photoID}") { photo(call) }
                get("/event/{eventID}") { event(call) }

            }
        }
    }

    private suspend fun snapshot(call: ApplicationCall) {
        call.respondHtmlTemplate(
            DoorTemplate(DoorTemplate.SNAPSHOT).apply {
            }
        ) {
            pageTitle { +"Photo" }
            content {
                div("imgContainer") {
                    img(alt = "photo", src = Door.snapshotURL, classes = "fullSize grey")
                }
            }
        }
    }

    private suspend fun stream(call: ApplicationCall) {
        call.respondHtmlTemplate(
            DoorTemplate(DoorTemplate.STREAM).apply {
            }
        ) {
            pageTitle { +"Stream" }
            content {
                div("imgContainer") {
                    img(alt = "stream", src = Door.streamURL, classes = "fullSize grey")
                }
            }
        }

    }

    /**
     * Shows photos taken either because the bell was rung or the proximity sensor fired.
     */
    private suspend fun history(call: ApplicationCall) {

        val dayParam = call.optionalIntParameter("day", -1)

        suspendTransaction {

            val allEvents = Event.all().orderBy(Pair(Events.time, SortOrder.DESC))

            val filterDate: LocalDate = if (dayParam >= 0) {
                LocalDate.now().minusDays(dayParam.toLong())
            } else {
                val lastEvent = allEvents.firstOrNull()
                if (lastEvent == null) {
                    LocalDate.now()
                } else {
                    lastEvent.time.toLocalDate()
                }
            }

            val dayOffset = Duration.between(filterDate.atStartOfDay(), LocalDate.now().atStartOfDay()).toDays().toInt()
            val events = allEvents.filter { it.time.toLocalDate() == filterDate }



            call.respondHtmlTemplate(
                DoorTemplate(DoorTemplate.HISTORY).apply {
                }
            ) {
                pageTitle { +"History" }
                content {
                    h1 {
                        a(href = "/history?day=${dayOffset + 1}", classes = "day") { +"<" }
                        if (dayOffset == 0) {
                            +"Today"
                        } else if (dayOffset == 1) {
                            +"Yesterday"
                        } else {
                            +dayFormat.format(filterDate)
                        }
                        if (dayOffset > 0) {
                            a(href = "/history?day=${dayOffset - 1}", classes = "day") { +">" }
                        } else {
                            a(href = "/history?day=${dayOffset}", classes = "day") { +"" }
                            +" "
                        }
                    }


                    div(classes = "thumbnails") {
                        for (event in events) {

                            val photo = event.photos.firstOrNull()

                            div(classes = "thumbnailBox") {
                                div(classes = "thumbnail") {
                                    if (photo != null) {
                                        a(href = "event/${event.id.value}", classes = "thumbnail") {
                                            img(
                                                alt = "photo",
                                                src = "/photo/${photo.id.value}",
                                                classes = "thumbnail grey"
                                            )
                                        }
                                        br()
                                    }

                                    span(classes = "time") {
                                        +timeFormat.format(event.time)
                                    }

                                    if (event.bell) {
                                        img(alt = "Bell", src = "/style/bell.png", classes = "icon")
                                    }

                                    if (event.proximity) {
                                        img(alt = "Proximity", src = "/style/proximity.png", classes = "icon")
                                    }

                                    if (event.manual) {
                                        img(alt = "Manual", src = "/style/manual.png", classes = "icon")
                                    }
                                }
                            }

                        }
                    }

                }

            }

        }
    }

    private suspend fun event(call: ApplicationCall) {
        val eventId = call.requiredIntParameter("eventID")

        suspendTransaction {
            val event = Event.findById(eventId) ?: throw NotFoundException("Event $eventId not found")


            call.respondHtmlTemplate(
                DoorTemplate(DoorTemplate.HISTORY).apply {
                }
            ) {
                pageTitle { +"Event" }
                content {
                    h1 {
                        span(classes = "time") { +timeFormat.format(event.time) }
                        +" "
                        span(classes = "date") { +dateFormat.format(event.time) }
                    }
                    div(classes = "thumbnails") {
                        for (photo in event.photos) {
                            div(classes = "thumbnailBox") {
                                div(classes = "thumbnail") {
                                    a(href = "/photo/${photo.id.value}") {
                                        img(alt = "photo", src = "/photo/${photo.id.value}", classes = "thumbnail grey")
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private suspend fun photo(call: ApplicationCall) {
        val photoID = call.requiredIntParameter("photoID")

        suspendTransaction {
            val photo = Photo.findById(photoID) ?: throw NotFoundException("Photo $photoID not found")

            val file = File(photo.filename)
            if (file.exists()) {
                call.respondFile(file)
            } else {
                println("Photo $photoID ($file) not found")
                throw NotFoundException("Photo #$photoID (${file.name}) not found")
            }
        }
    }

    private suspend fun manual(call: ApplicationCall) {
        trigger(TriggerType.MANUAL)
        call.respondRedirect("/history")
    }

    companion object {

        val dateFormat = DateTimeFormatter.ofPattern("EE dd MMM")
        val timeFormat = DateTimeFormatter.ofPattern("kk:mm")
        val dayFormat = DateTimeFormatter.ofPattern("EE dd")

        const val MINUTE: Int = 60
        const val HOUR: Int = MINUTE * 60
        const val DAY: Int = HOUR * 24

    }

}
