package uk.co.nickthecoder.door

import io.ktor.http.content.*
import io.ktor.routing.*

fun Route.staticResources(remotePath: String, resourcePackage: String): Route =
    static(remotePath) { resources(resourcePackage) }

