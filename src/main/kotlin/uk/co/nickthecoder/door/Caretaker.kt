package uk.co.nickthecoder.door

import org.jetbrains.exposed.sql.transactions.transaction
import java.io.File
import java.time.LocalDate

object Caretaker {

    private var thread: CaretakerThread? = null
    private var isEnding = false

    fun start() {
        thread = CaretakerThread().apply {
            start()
        }
    }

    /**
     * Prevents the Caretaker from starting a new (daily) run.
     * If the caretaker is currently performing its (daily) [task], then we join with its thread, ]
     * so that [stop] does not return until the (daily) [task] has finished.
     */
    fun stop() {
        isEnding = true
        thread?.interrupt()
        thread?.join()
        thread = null
    }

    /**
     * Using the default of [Door.caretakerSleepMillis], this is called once a day.
     */
    fun task() {

        val files = Door.photosDirectory.listFiles()
        val photoFilenames = transaction {
            Photo.all().map { it.filename }.toSet()
        }
        //println("CARETAKER: Known files : $photoFilenames")
        //println("CARETAKER: Actual Files : ${files.toList()}")

        files?.forEach { file ->
            if (file.extension == "jpg" && !photoFilenames.contains(file.path)) {
                println("CARETAKER: Deleting photo : $file")
                file.delete()
            }
        }

        val minDate = LocalDate.now().minusDays(Door.historyDays.toLong()).atStartOfDay()
        transaction {
            val oldEvents = Event.find { Events.time.lessEq(minDate) }
            println("CARETAKER: There are ${oldEvents.count()} old events to remove")
            for (event in oldEvents) {
                println("CARETAKER: Deleting old event ${Door.dateTimeFormatter.format(event.time)}")
                event.photos.forEach { photo ->
                    try {
                        File(photo.filename).delete()
                    } catch (e: Exception) {
                        // Do nothing
                    }
                    photo.delete()
                }
                event.delete()
            }
        }
        println("CARETAKER: Finished")
    }


    private class CaretakerThread : Thread("caretaker") {

        init {
            isDaemon = true
        }

        override fun run() {
            try {
                while (!isEnding) {
                    task()
                    if (!isEnding) {
                        sleep(Door.caretakerSleepMillis)
                    }
                }
            } catch (e: InterruptedException) {
                println("CARETAKER: Interrupted")
                // Do nothing
            }
        }
    }

}
