package uk.co.nickthecoder.door

import com.pi4j.Pi4J
import com.pi4j.io.gpio.digital.DigitalInput
import com.pi4j.io.gpio.digital.DigitalOutput
import com.pi4j.io.gpio.digital.DigitalState
import uk.co.nickthecoder.door.util.listen

private fun DigitalState.not() = DigitalState.inverseState(this)

object GPIO {

    private class State {

        val pi4j = Pi4J.newAutoContext()

        val bellButton = pi4j.create(
            DigitalInput.newConfigBuilder(pi4j)
                .id("bell")
                .name("Bell")
                .address(Door.bellPin)
                .debounce(3000L)
                .provider("pigpio-digital-input")
        )

        val proximitySensor = pi4j.create(
            DigitalInput.newConfigBuilder(pi4j)
                .id("proximity")
                .name("Proximity")
                .address(Door.proximityPin)
                .provider("pigpio-digital-input")
        )

        val lights = pi4j.create(
            DigitalOutput.newConfigBuilder(pi4j)
                .id("lights")
                .name("Lights")
                .address(Door.lightsPin)
                .shutdown(DigitalState.LOW)
                .initial(DigitalState.LOW)
                .provider("pigpio-digital-output")
        )

        init {
            // The bell has a pull up resistor. A low value indicates the button has been pushed.
            bellButton.listen { event ->
                if (event.state() == DigitalState.LOW) {
                    trigger(TriggerType.BELL)
                }
            }

            proximitySensor.listen { event ->
                if (event.state() == DigitalState.HIGH) {
                    trigger(TriggerType.PROXIMITY)
                }
                // Turn the lights on/off to the same value as the proximity sensor's value.
                // TODO If we add more hardware, we could check the ambient light levels,
                // and only turn the lights on when dark.
                // Or we could use the time of day and day of the year to decide if is is dark. Hmm.
                lights.state(event.state())
            }
        }
    }


    private var state: State? = null

    // Turn the lights on/off
    var lights: Boolean
        get() = state?.lights?.isHigh ?: false
        set(value) {
            state?.lights?.state(if (value) DigitalState.HIGH else DigitalState.LOW)
        }

    fun start() {

        state = State()
        Runtime.getRuntime().addShutdownHook(Thread {
            stop();
        })

    }

    fun stop() {
        state?.pi4j?.shutdown()
        state = null
    }

    fun lightsOnFor(millis: Long) {
        Thread(
            {
                lights = true
                Thread.sleep(millis)
                lights = false
            },
            "lights"
        ).start()
    }
}
